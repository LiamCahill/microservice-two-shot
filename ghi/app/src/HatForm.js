import React from "react";


class HatForm extends React.Component { async
    constructor(props) {
        super(props);
        this.state = {
            style: '',
            fabric: '',
            color: '',
            pictureURL: '',
            location: '',
            locations: [],
        }
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.locations
        
        const locationsUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
        },
    }
    const response = await fetch(locationsUrl, fetchConfig);
    if (response.ok) {
        const newLocation = await response.json();
        const cleared = {
            style: '',
            fabric: '',
            color: '',
            pictureURL: '',
            location: '',
        };
        this.setState(cleared);
    }
}



handleStyleChange(event) {
    const value = event.target.value;
    this.setState({style: value})
}
handleFabricChange(event) {
    const value = event.target.value;
    this.setState({fabric: value})
}
handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
}
handlePictureUrlChange(event) {
    const value = event.target.value;
    this.setState({pictureURL: value})
}
handleLocationChange(event) {
    const value = event.target.value;
    this.setState({location: value})
}

async componentDidMount() {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json();
        console.log(" hahahahahha", data)
        this.setState({locations: data.locations});

    }
}

render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input value={this.state.style} onChange={this.handleStyleChange} placeholder="Style" name="style" required type="text" id="style" className="form-control" />
                        <label htmlFor="style">Style</label>
                    </div>
                    <div className="form-floating mb-3 datepicker">
                        <input value={this.state.fabric} onChange={this.handleFabricChange} placeholder="Fabric" name="fabric" required type="text" id="fabric" className="form-control"/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3 datepicker">
                        <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" name="color" required type="text" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <textarea value={this.state.pictureUrl} onChange={this.handlePictureUrlChange} className="form-control" id="pictureURL" placeholder="Picture url" name="pictureURL" rows="3"></textarea>
                        <label htmlFor="pictureUrl">Picture url</label>
                    </div>

                    <div className="mb-3">
                        <select value={this.state.location} onChange={this.handleLocationChange} required id="location" name="location" className="form-select" >
                            <option value="">Choose a location</option> 
                            {this.state.locations.map(location => {
                                return (
                                    <option value={location.href} key={location.href}>
                                       {location.closet_name}-{location.shelf_number}/{location.shelf_number}
                                    </option>
                                )
                            })}
                        </select>
                    </div>

                    <button className="btn btn-primary">Create a hat</button>
                </form>
            </div>
        </div>
    </div>

);
}

}


export default HatForm;