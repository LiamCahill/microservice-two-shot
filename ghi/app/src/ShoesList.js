import React from "react";

function ShoesList(props) {
    return (
      <table className = "table table-striped">
        <thead>
          <tr>
            <th>Shoes</th>
            <th>Bins</th>
          </tr>
        </thead>
        <tbody>
        {props.shoes.map(shoe => {
          return (
            <tr key={shoe.id}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.bin.closet_name }</td>
            </tr>
          );
        })
        }
        </tbody>
      </table>
    );
  }
  
  export default ShoesList;
