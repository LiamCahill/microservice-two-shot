import React from 'react'

class NewShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            model_name:'',
            color: '',
            imageURL: '',
            bin: '',
            bins: [],
        }
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleModelNameChange = this.handleModelNameChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleimageURLChange = this.handleimageURLChange.bind(this)
        this.handleBinChange = this.handleBinChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}

        delete data.bins

        console.log(data)
        const binUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(binUrl, fetchConfig)
        if (response.ok) {
            const newBin = await response.json()
            const cleared = {
                manufacturer: '',
                model_name:'',
                color: '',
                imageURL: '',
                bin: '',
                // [],
            }
            this.setState(cleared)
        }
    }

    handleManufacturerChange(event) {
        const value = event.target.value
        this.setState({manufacturer: value})
    }

    handleModelNameChange(event) {
        const value = event.target.value
        this.setState({model_name: value})
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})
    }

    handleimageURLChange(event) {
        const value = event.target.value
        this.setState({imageURL: value})
    }

    handleBinChange(event) {
        const value = event.target.value
        this.setState({bin: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
    
        const response = await fetch(url);
        
        if (response.ok) {
        const data = await response.json()
        this.setState({bins: data.bins})
        }
    }

    render() {
    return (
        <div className="my-5 container">
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
                <input value={this.state.manufacturer} onChange={this.handleManufacturerChange} placeholder="Manufacturer" name="manufacturer" required type="text" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3 datepicker">
                <input value={this.state.model_name} onChange={this.handleModelNameChange} placeholder="Model name" name="model_name" required type="text" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model name</label>
            </div>
            <div className="form-floating mb-3 datepicker">
                <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" name="color" required type="text" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
                <input value={this.state.imageURL} onChange={this.handleimageURLChange} className="form-control" id="imageURL" placeholder="imageURL" name="imageURL" required type="url"></input>
                <label htmlFor="imageURL">imageURL</label>
            </div>
            <div className="mb-3">
                <select value={this.state.bin} onChange={this.handleBinChange} required id="bin" name="bin" className="form-select">
                <option value="">Bin options</option>
                {this.state.bins.map(bin => {
                        return (
                            <option value={bin.href} key={bin.href}>
                                {bin.closet_name}{bin.bin_number}{bin.bin_number}
                            </option>
                        )
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    );
    }
}

export default NewShoeForm
