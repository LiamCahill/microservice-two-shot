import React from 'react';

function HatsList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Hats</th>
            <th>Locations</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color}</td>
                <td>{ hat.location.closet_name} </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  export default HatsList;