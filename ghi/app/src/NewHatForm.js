import React from 'react'

class NewHatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style_name:'',
            color: '',
            pictureURL: '',
            location: '',
            locations: [],
        }
        this.handleFabricChange = this.handleFabricChange.bind(this)
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handlepictureURLChange = this.handlepictureURLChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        data.pictureurl = data.pictureurl
        delete data.locations
        delete data.pictureurl
        console.log(data)
        const locationUrl = 'http://localhost:8090/api/locations/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()
            const cleared = {
                fabric: '',
                style_name:'',
                color: '',
                pictureURL: '',
                location: '',
                // [],
            }
            this.setState(cleared)
        }
    }

    handleFabricChange(event) {
        const value = event.target.value
        this.setState({manufacturer: value})
    }

    handleStyleNameChange(event) {
        const value = event.target.value
        this.setState({style_name: value})
    }

    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})
    }

    handlepictureURLChange(event) {
        const value = event.target.value
        this.setState({pictureURL: value})
    }

    handleLocationChange(event) {
        const value = event.target.value
        this.setState({location: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
    
        const response = await fetch(url);
        
        if (response.ok) {
        const data = await response.json()
        this.setState({bins: data.bins})
        }
    }

    render() {
    return (
        <div className="container">
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
                <input value={this.state.manufacturer} onChange={this.handleManufacturerChange} placeholder="Fabric" name="fabric" required type="text" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
                <input value={this.state.style_name} onChange={this.handleModelNameChange} placeholder="Style name" name="style_name" required type="text" id="style_name" className="form-control"/>
                <label htmlFor="model_name">Style name</label>
            </div>
            <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" name="color" required type="text" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
                <input value={this.state.pictureURL} onChange={this.handleimageURLChange} className="form-control" id="pictureURL" placeholder="pictureURL" name="pictureURL" required type="url"></input>
                <label htmlFor="pictureURL">pictureURL</label>
            </div>
            <div className="mb-3">
                <select value={this.state.location} onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                <option value="">Location options</option>
                {this.state.locations.map(location => {
                        return (
                            <option value={location.href} key={location.href}>
                                {location.closet_name}{location.shelf_number}{location.shelf_number}
                            </option>
                        )
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    );
    }
}

export default NewHatForm