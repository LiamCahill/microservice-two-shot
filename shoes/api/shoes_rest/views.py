from email.mime import message
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href","closet_name","bin_number", "bin_size"]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'imageURL',
        'bin'
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(['GET', 'POST'])
def api_shoes(request): #Better name? List of Shoes? , BinVO_ID=None

    if request.method == 'GET':
        shoes = Shoe.objects.all()
        return JsonResponse(
            shoes,
            encoder=ShoeEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin does not exist!"},
                status=400
            )
        shoe = Shoe.objects.create(**content)  # Verify what content

        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )


@require_http_methods(['DELETE'])
def api_shoe(request, pk): #Better name? Single Shoe?
    if request.method == 'DELETE':
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({'message': 'Does not exist'})
    # Create your views here.
