from unicodedata import name  # Delete no import
from django.db import models
# from wardrobe.api.wardrobe_api import Bin


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField(default=1)
    bin_size = models.PositiveSmallIntegerField(default=1)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    imageURL = models.URLField(blank=True, null=True)
    bin = models.ForeignKey(BinVO,
                            related_name="shoe",
                            on_delete=models.PROTECT)
# Create your models here.
