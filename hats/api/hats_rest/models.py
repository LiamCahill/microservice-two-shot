from django.db import models



class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(default=1)
    shelf_number = models.PositiveSmallIntegerField(default=1)


class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    pictureURL = models.URLField(blank=True, null=True)
    location = models.ForeignKey(LocationVO, related_name='Hats', on_delete=models.PROTECT)




